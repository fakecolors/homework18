#include <iostream>
#include <cassert>

class Stack
{
private:
	int x = 8;
	int* array = new int[x];
	int i_next;

public:

	void reset() //All elements of array = 0;
	{
		i_next = 0;
		for (int i = 0; i < x; ++i)
			array[i] = 0;
	}

	bool push(int value)
	{
		if (i_next == x)
		{
			std::cout << "Stack is full!" << '\n';
			return false;
		}

		array[i_next++] = value;
		std::cout << "We added " << value << " to our stack" << '\n';
		return true;
	}

	int pop()
	{
		assert(i_next > 0);

		// int a = array[--i_next]; 
		// --i_next; 
		// return a; 

		int& i = array[--i_next];
		array[++i_next];
		std::cout << "We delete " << i << " from our stack" << '\n';

		return array[--i_next];
	}

	void print()
	{
		std::cout << "Our stack = [ ";
		for (int i = 0; i < i_next; ++i)
			std::cout << array[i] << ' ';
		std::cout << "]\n\n";
	}
};

int main()
{
	Stack stack;
	stack.reset();
	stack.print();

	stack.push(1);
	stack.push(2);
	stack.push(3);
	stack.push(4);
	stack.push(5);
	stack.print();

	stack.push(6);
	stack.push(7);
	stack.push(8);
	stack.print();

	stack.push(9);
	stack.print();

	stack.pop();
	stack.pop();
	stack.pop();
	stack.print();

	stack.pop();
	stack.pop();
	stack.pop();
	stack.pop();
	stack.pop();
	stack.print();

	//stack.pop();
	//Checking error message [line: 35]

	return 0;
}